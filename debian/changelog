ruby-pdf-reader (2.11.0-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster
  * Update standards version to 4.5.1, no changes needed.

  [ Aquila Macedo ]
  * New upstream version 2.11.0
  * d/control: bumps Standards-Version to 4.6.2.
  * d/patches: rebase patches and add Forwarded field.
  * d/s/lintian-overrides: fix missing source file.
  * d/ruby-pdf-reader.lintian-overrides: override lintian warnings for
    files that are not documentation.

 -- Aquila Macedo Costa <aquilamacedo@riseup.net>  Mon, 09 Oct 2023 11:42:26 -0300

ruby-pdf-reader (2.4.1-1) unstable; urgency=medium

  * New upstream version 2.4.1
    + improving copyright handling upstream of AFM fonts
    + drop 0001-add-licensefile.patch
  * Update years and my email address in copyright file

 -- Cédric Boutillier <boutil@debian.org>  Thu, 24 Sep 2020 15:51:54 +0200

ruby-pdf-reader (2.4.0-1) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Cédric Boutillier ]
  * New upstream version 2.4.0
  * Refresh packaging with dh-make-ruby -w
    + Bump Standards-Version to 4.5.0 (no changes needed)
    + Set debhelper-compat to 13
    + gem install layout
  * Https in homepage field and copyright-formal url

 -- Cédric Boutillier <boutil@debian.org>  Tue, 01 Sep 2020 22:32:48 +0200

ruby-pdf-reader (2.1.0-1) unstable; urgency=medium

  * New upstream version 2.1.0
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Use salsa.debian.org in Vcs-* fields
  * Bump debhelper compatibility level to 11
  * Bump Standards-Version to 4.2.1 (no changes needed)
  * Drop obsoloete 0002-examples_rubygems.patch
  * Adapt to the new md extension of the README file

 -- Cédric Boutillier <boutil@debian.org>  Sun, 30 Sep 2018 22:03:32 +0200

ruby-pdf-reader (1.4.0-2) unstable; urgency=medium

  * Team upload.
  * Include missing file for Type1 fonts license compliance,
    and update debian/copyright. (Closes: #850688)

 -- Christian Hofstaedtler <zeha@debian.org>  Sun, 05 Feb 2017 19:10:44 +0000

ruby-pdf-reader (1.4.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release (Closes: #795763).
    - The new upstream version updates its assertion configuration
      for the rspec 3 version, that doesn't allow the use of :stdlib anymore.
  * debian/compat:  Bump to version 9, fix lintian warning.
  * debian/control:
    - Update vcs-git link to https, fix lintian warning.
    - Update vcs-browser link to https, fix lintian warning.
    - Update standards-version to 3.9.7, fix lintian warning.
    - Update debhelper to require version >= 9.
  * debian/copyright: Update year format.

 -- Lucas Albuquerque Medeiros de Moura <lucas.moura128@gmail.com>  Tue, 01 Mar 2016 16:40:53 -0300

ruby-pdf-reader (1.3.3-1) unstable; urgency=medium

  * Imported Upstream version 1.3.3
  * debian/control:
    + remove obsolete DM-Upload-Allowed flag
    + use canonical URI in Vcs-* fields
    + bump Standards-Version to 3.9.5 (no changes needed)
    + add dependencies on ruby-ttfunk, ruby-afm and ruby-hashery
    + change my email address to @d.o
  * Use rake method to run the tests
  * debian/copyright: use DEP5 copyright-format/1.0 URL for Format field
  * Update debian/watch. Thanks Bart Martens

 -- Cédric Boutillier <boutil@debian.org>  Tue, 18 Feb 2014 18:22:31 +0100

ruby-pdf-reader (1.1.1-2) unstable; urgency=low

  * Bump build dependency on gem2deb to >= 0.3.0~

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Tue, 26 Jun 2012 14:44:29 +0200

ruby-pdf-reader (1.1.1-1) unstable; urgency=low

  * New upstream version

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Fri, 11 May 2012 00:18:40 +0200

ruby-pdf-reader (1.1.0-1) unstable; urgency=low

  * New upstream version

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Thu, 29 Mar 2012 16:44:57 +0200

ruby-pdf-reader (1.0.0-2) unstable; urgency=low

  * Added missing dependency on ruby-rc4 (Closes: #665266)

 -- Gunnar Wolf <gwolf@debian.org>  Tue, 27 Mar 2012 17:04:58 -0600

ruby-pdf-reader (1.0.0-1) unstable; urgency=low

  * New upstream version
  * Add dependency on ruby-rc4
  * Drop 0003-examples_fix_shebang.patch and 0005_fix_rspec_example.patch
    + not needed anymore
  * Add 0006-spec_add_require_yaml.patch
    + solves missing dependency on yaml to run spec/integration_spec.rb
  * Bumps Standards-Version to 3.9.3 (no change needed)
  * Change debian/watch to point directly to github.com

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Thu, 23 Feb 2012 16:35:50 +0100

ruby-pdf-reader (0.10.1-1) unstable; urgency=low

  * Initial release (Closes: #640670)

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Wed, 07 Sep 2011 09:11:53 +0200
